# Two Day Python Workshop 

- Venue: Tamil Sangam
- Rent: Rs. 3400/-
- Days: 2 
- Session Time: 1.5 hours?
- Sessions per day: 4
- Lunch, Tea & Biscuits provided 
- Seating capacity: 80-100
 
# Economics 
  
##  Scale 40
-  Tea + Biscuit  = Rs. 40/- (2 x 2 x 10)
- Lunch  = Rs. 80/- (2 x 40) 
- Certificate = Rs. 15/-
- Sticker = Rs. 10/-
- Advertisement = Rs. 20/-
- Rent = Rs. 150/-
- Projector = Rs. 20/-
 
## Total Cost = Rs. 335/-
## Workshop Fees = Rs. 400/-
  
# Schedule

- Need to insert lightning talks | Day 2 before lunch has some free time
- Need more speakers

## Day 1

| Time Slot | Session | Speaker |
| --- | --- | --- |
| 9:30 - 11:00 |  Introduction, Linux Fundamentals | Prasanna Venkadesh |
| 11:00 - 11:15 | Tea Break | Food Committee | 
| 11:15 - 13:00 | Python 1 | | 
| 13:00 - 14:00 | Lunch | Food Committee|
| 14:00 - 15:30 | Python 2 | |
| 15:30 - 15:45 | Tea Break | Food Committee |
| 15:45 - 17:00 | Python 3 | | 

## Day 2

| Time Slot | Session | Speaker |
| --- | --- | --- |
| 9:00 - 10:30 |  Python 4 |  |
| 10:30 - 10:45 | Tea Break | Food Committee | 
| 10:45 - 12:15 | Python 5 | | 
| 12:15 - 13:15 | Lunch | Food Committee|
| 13:15 - 15:00 | Valedictory, Wrapping up, Conclusion | |
 
# Syllabus
## Linux Fundamentals
- What is FOSS?
- What is Linux?

## Python 1

## Python 2 

## Python 3

## Python 4

## Python 5


## Lightning Talks
- Privacy & Decentralization
- Alternative Applications

# Outreach & Registration

## Site & Form

## Posters 

## Past participants & Acquaintance

## Social Media 
- Daily engagement with Python & workshop go to

## Newspaper 

## Registration



# Job Distribution
## Roles 
- 1 x Speaker
- 1 x Speaker Volunteer
- 8 x Team Leader
- 1 x Organizer 
- 2 x Organizing Volunteer

## Volunteers
| Name | Committee |  Talk | Team |
| --- | --- | --- | --- |
| Prasanna | Speaker, Outreach | Linux Fundamentals | |
| Kamalavelan | Food, Organizing | | | 
| Arunekumar | Speaker, Outreach | | |
| Ragul | Speaker, Distro, Site | | |
| Sarath | Outreach | | | 
| Kavine | Outreach | | |
| Noordine | Outreach | | | 
| Maniraj | | | | 
| Manimaran | | | | 
| Harish Nawaaz | | | | 
| Infant | | | | 
### Unconfirmed

- Vignaraj 
- Viji (VGLUG)
- Vimal (VGLUG)

# Distro
- ArchLinux
- Atom
- IPython
- Fish
- Git
- DE? 
- pyttest
- 


# Freebies & Schwag

- ID tag
- Sticker

